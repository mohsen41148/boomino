# BOOMINO FRONT END TASK

Powered by [Vite js](https://vitejs.dev).

### Commands:

#### Run in development mode
`npm run dev`

#### Build in production mode
`npm run build`

#### Vite preview

`npm run preview`


### Quick run:

`git clone git@gitlab.com:mohsen41148/boomino.git `

`cd boomino`

`npm i` or `yarn add`

`npm unr dev`


Enjoy :)